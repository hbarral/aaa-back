<?php

use Faker\Generator as Faker;


$factory->define(App\Contact::class, function (Faker $faker) {

    return [
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'username' => $faker->userName
    ];
});
